package com.example.kote.tsuandroidproject.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kote.tsuandroidproject.R;
import com.squareup.picasso.Picasso;

public class SecondActivity extends AppCompatActivity {
    private static final String TAG = "SecondActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        final String url = "https://image.tmdb.org/t/p/w640";
        Intent intent = getIntent();
        Picasso.with(this).load(url + intent.getStringExtra("poster")).into((ImageView) findViewById(R.id.imgView));
        ((TextView)findViewById(R.id.full_description)).setText(intent.getStringExtra("overview"));
    }
}
