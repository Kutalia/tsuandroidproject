package com.example.kote.tsuandroidproject;

import android.view.View;

import com.example.kote.tsuandroidproject.model.Movie;

import java.util.List;

public interface RecyclerViewOnItemClick {
    public void onItemClick(View view, int position, List<Movie> Movies);
}
