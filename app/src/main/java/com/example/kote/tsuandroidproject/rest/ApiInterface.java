package com.example.kote.tsuandroidproject.rest;

import com.example.kote.tsuandroidproject.model.MoviesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    @GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);

    @GET("discover/movie")
    Call<MoviesResponse> getClassicSlashers(@Query("with_genres") String genre, @Query("with_keywords") String keyword, @Query("primary_release_date") String fromDate,
                                         @Query("primary_release_date.lte") String toDate, @Query("api_key") String apiKey);
}