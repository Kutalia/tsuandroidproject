package com.example.kote.tsuandroidproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.kote.tsuandroidproject.R;
import com.example.kote.tsuandroidproject.RecyclerViewOnItemClick;
import com.example.kote.tsuandroidproject.adapter.MoviesAdapter;
import com.example.kote.tsuandroidproject.model.Movie;
import com.example.kote.tsuandroidproject.model.MoviesResponse;
import com.example.kote.tsuandroidproject.rest.ApiClient;
import com.example.kote.tsuandroidproject.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();


    private final static String API_KEY = "169a2f27fb42b8387fb65dc9d396de91";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Classic Slasher Movies");

        if (API_KEY.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please obtain your API KEY from themoviedb.org first!", Toast.LENGTH_LONG).show();
            return;
        }

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<MoviesResponse> call = apiService.getClassicSlashers("27", "12339,10714", "1976", "2000&sort_by=vote_average.desc", API_KEY);
        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                int statusCode = response.code();
                List<Movie> movies = response.body().getResults();
                recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext(), new RecyclerViewOnItemClick() {
                    @Override
                    public void onItemClick(View view, int position, List<Movie> movies) {
                        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                        intent.putExtra("overview", movies.get(position).getOverview());
                        intent.putExtra("poster", movies.get(position).getPosterPath());
                        startActivity(intent);
                    }
                }));
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }
}
